<?php
namespace Excellence\Crud\Block;

class Show extends \Magento\Framework\View\Element\Template
{
    protected $crudFactory;

    public $_coreRegistry;

    public function __construct(\Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Element\Template\Context $context,
        \Excellence\Crud\Model\CrudFactory $dataFactory
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->crudFactory = $dataFactory;
        parent::__construct($context);
    }
    public function getUserData()
    {
       
        //get values of current page. if not the param value then it will set to 1
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        //get values of current limit. if not the param value then it will set to 5
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 5;
        $collection = $this->crudFactory->create();
        $sortOrder = ($this->getRequest()->getParam('sortOrder')) ? $this->getRequest()->getParam('sortOrder') : 0;
        $sortBy = ($this->getRequest()->getParam('sortBy')) ? $this->getRequest()->getParam('sortBy') : '';

        if ($sortBy) {
            $collection = $collection->getCollection();
            $collection->setOrder($sortBy, $sortOrder);
            $collection->setPageSize($pageSize);
            $collection->setCurPage($page);
            return $collection;
        } else {
            $collection = $collection->getCollection();
            $collection->setPageSize($pageSize);
            $collection->setCurPage($page);
            return $collection;

        }
    }
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Employee Records'));

        if ($this->getUserData()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'fme.news.pager'
            )->setAvailableLimit(array(5 => 5, 8 => 8, 10 => 10))->setShowPerPage(true)->setCollection(
                $this->getUserData()
            );
            $this->setChild('pager', $pager);
            $this->getUserData()->load();
        }
        return $this;
    }
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
    public function getSearchData()
    {
        $title = ($this->getRequest()->getParam('search')) ? $this->getRequest()->getParam('name') : '';
        $tempSearch = $this->crudFactory->create();
        $data = $tempSearch->loadByTitle($title);
        return $data;
    }
}
