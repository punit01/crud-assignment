<?php
namespace Excellence\Crud\Model\ResourceModel;
 
class Crud extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('crud', 'crud_id'); // 'crud_id' is table primary key
    }
    public function loadByTitle($title){
        $table = $this->getMainTable();
        $where = $this->getConnection()->quoteInto("name = ?", $title);
        $sql = $this->getConnection()->select()->from($table,array('crud_id'))->where($where);
        $id = $this->getConnection()->fetchOne($sql);
        return $id;
    }
}
